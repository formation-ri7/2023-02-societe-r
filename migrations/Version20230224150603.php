<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230224150603 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE employee_id_seq CASCADE');
        $this->addSql('DROP TABLE employee');
        $this->addSql('ALTER TABLE person ADD discr VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE person ADD started_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE person ADD salary DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN person.started_at IS \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE employee_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE employee (id INT NOT NULL, started_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, salary DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN employee.started_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE person DROP discr');
        $this->addSql('ALTER TABLE person DROP started_at');
        $this->addSql('ALTER TABLE person DROP salary');
    }
}
