<?php

namespace App\Controller\Api;

use App\Repository\EmployeeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmployeeApiController extends AbstractController
{
    public function __construct(private readonly EmployeeRepository $employeeRepository)
    {
    }

    #[Route('/api/employee/more-than-40-old', name: 'app_employee_more_than_40_old')]
    public function employeesMoreThan40Old(): Response
    {
        return $this->json([
            'employees' => $this->employeeRepository->findByAgeGreaterThan(40),
        ]);
    }
}
