<?php

namespace App\DataFixtures;

use App\Entity\Skill;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SkillFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {

        $skillArray = array(
            'skill1' => 'PHP',
            'skill2' => 'Javascript',
            'skill3' => 'Python',
            'skill4' => 'SQL',
            'skill5' => 'HTML/CSS',
            'skill6' => 'Docker',
            'skill7' => 'Kubernetes',
            'skill8' => 'Project analyse',
            'skill9' => 'Symfony',
            'skill10' => 'Vue',
            'skill11' => 'React',
            'skill12' => 'Angular',
            'skill13' => 'Svelte'
        );
        // $product = new Product();
        // $manager->persist($product);


        foreach ($skillArray as $key => $value) {
            $skill = new Skill();
            $skill->setCode(strtolower($value));
            $skill->setLabel($value);
            $skill->setDescription('Connaissance en ' . $value);
            $manager->persist($skill);
        }

        $manager->flush();


    }
}