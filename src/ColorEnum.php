<?php

namespace App\Entity;

enum ColorEnum: string
{
    case blue = 'blue';

    case red = 'red';

    case green = 'green';
}
